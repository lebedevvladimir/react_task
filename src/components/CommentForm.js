import React, { Component } from 'react'

export default class CommentForm extends Component {
  render () {
    let hasError = this.props.isCommentValid ? '' : ' is-invalid'
    return (
      <form onSubmit={this.props.submitForm}>
        <div className='media'>
          <a className='pull-left mr-3' href='#'>
            <img className='media-object rounded-circle' src='https://placeimg.com/40/40/any' alt='img' />
          </a>
          <input type='text' className={'form-control col-11' + hasError} placeholder='Add a comment...'
            onChange={this.props.handleChange}
            value={this.props.formValue} />
          {!this.props.isCommentValid &&
          <div className='text-danger position-absolute'
            style={{
              left: '300px',
              top: '0px'
            }}
          >comment should be less than 25</div>}
        </div>
      </form>
    )
  }
}
