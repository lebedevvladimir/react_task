import React, { Component } from 'react'

// import Comment from "../components/Comment";

/**
 * Main Wrapper
 */
class CommentsList extends Component {
  render () {
    let comments = this.props.comments || []
    return (
      <div className='media-list'>
        {comments.map((comment, index) => {
          return (
            <div className='mb-3' key={index}>
              <div className='media' key={index}>
                <a className='pull-left mr-3' href='#'>
                  <img className='media-object rounded-circle' src='https://placeimg.com/40/40/any' />
                </a>
                <div className='media-body'>
                  <div className='small text-muted'>{comment.user.name}</div>
                  <div>{comment.message}</div>
                </div>

              </div>

              <a className='small mr-1' href=''><u>Likes</u>{`(${comment.likes})`}</a>
              <a className='small mr-1' href=''><u>Reply</u></a>
              <a className='small mr-1' href=''><u>View Replies</u>{`(${comment.replies})`}</a>
            </div>
          )
        })
        }
      </div>
    )
  }
}

export default CommentsList
