import {
  INSERT_COMMENT,
  COMMENT_IS_NOT_VALID,
  SHOW_LOADER,
  CLOSE_LOADER,
  CHANGE_FORM_VALUE
} from './constants'

export function insertComment (data) {
  return dispatch => {
    dispatch(showLoader())
    setTimeout(() => {
      if (data.length > 25) {
        dispatch({ type: COMMENT_IS_NOT_VALID })
      } else {
        dispatch({ type: INSERT_COMMENT, data: data })
      }
      dispatch(closeLoader())
    }, 1500)
  }
}

export function changeFormValue (data) {
  return { type: CHANGE_FORM_VALUE, data: data }
}

function showLoader () {
  return { type: SHOW_LOADER }
}

function closeLoader () {
  return { type: CLOSE_LOADER }
}
