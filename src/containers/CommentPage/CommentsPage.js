import React, { Component } from 'react'
import { connect } from 'react-redux'

import CommentForm from '../../components/CommentForm'
import CommentsList from '../../components/CommentsList'
import { changeFormValue, insertComment } from './actions'

/**
 * Main Wrapper
 */
class CommentsPage extends Component {
  constructor (props) {
    super(props)

    this.submitForm = this.submitForm.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  submitForm (event) {
    event.preventDefault()

    const { insertComment } = this.props
    insertComment(this.props.formValue)
  }
  handleChange (event) {
    this.props.changeFormValue(event.target.value)
  }

  render () {
    const {
      comments,
      isCommentValid,
      showLoader,
      formValue
    } = this.props

    return (
      <div>
        <header>Comments</header>
        <CommentForm submitForm={this.submitForm}
          isCommentValid={isCommentValid}
          formValue={formValue}
          handleChange={this.handleChange} />
        <hr />
        <CommentsList comments={comments} />
        {showLoader && <div className='position-absolute m-auto' style={{
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          width: '10px',
          height: '10px'
        }}>
          <i className='fa fa-spinner fa-spin text-info' style={{ fontSize: '50px' }} />
        </div>}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    comments: state.commentPage.comments,
    isCommentValid: state.commentPage.isCommentValid,
    showLoader: state.commentPage.showLoader,
    formValue: state.commentPage.formValue,
    state: state
  }
}
const mapDispatchToProps = {
  insertComment,
  changeFormValue
}
export default connect(mapStateToProps, mapDispatchToProps)(CommentsPage)
