import {
  COMMENT_IS_NOT_VALID,
  INSERT_COMMENT,
  SHOW_LOADER,
  CLOSE_LOADER,
  CHANGE_FORM_VALUE
} from './constants'

const initialState = {
  comments: [
    {
      user: {
        id: 12,
        name: 'Janet Ryan'
      },
      message: 'I really like',
      replies: 2,
      likes: 10
    }, {
      user: {
        id: 13,
        name: 'Alice Sunders'
      },
      message: 'blablabla',
      replies: 0,
      likes: 1
    }
  ],
  isCommentValid: true,
  showLoader: false,
  formValue: '',
  user: {
    id: 12,
    name: 'Janer Ryan'
  }
}

function commentReducer (state = initialState, action) {
  switch (action.type) {
    case INSERT_COMMENT:
      return Object.assign({}, state, {
        comments: state.comments.concat([{
          user: {
            id: state.user.id,
            name: state.user.name
          },
          message: action.data,
          replies: 0,
          likes: 0
        }]),
        isCommentValid: true,
        formValue: ''

      }
      )
    case COMMENT_IS_NOT_VALID:
      return Object.assign({}, state, { isCommentValid: false })
    case SHOW_LOADER:
      return Object.assign({}, state, { showLoader: true })
    case CLOSE_LOADER:
      return Object.assign({}, state, { showLoader: false })
    case CHANGE_FORM_VALUE:
      return Object.assign({}, state, { formValue: action.data })
    default:
      return state
  }
}

export default commentReducer
