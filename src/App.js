import React, { Component } from 'react'
import { connect } from 'react-redux'

import CommentsPage from './containers/CommentPage/CommentsPage'

/**
 * Main Wrapper
 */
class App extends Component {
  render () {
    return (
      <div className='container'>
        <CommentsPage />
      </div>
    )
  }
}

export default connect()(App)
