import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import { hashHistory } from 'react-router'
import commentReducer from './containers/CommentPage/reducer'

let initialState = {}

const Store = createStore(
  combineReducers({
    routing: routerReducer,
    commentPage: commentReducer
  }),
  initialState,
  compose(
    applyMiddleware(routerMiddleware(hashHistory)),
    applyMiddleware(thunk)
  )
)

export default Store
